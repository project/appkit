For DDev users whose node is included in environment:

Install dependencies
```
ddev exec -d /var/www/html/web/themes/custom/appkit npm i
```

Compiling the SCSS/JS written inside the source directory
```
ddev exec -d /var/www/html/web/themes/custom/appkit npm start
```

Enable the theme:
```
ddev drush theme:enable appkit
```

Set the theme as default:
```
ddev drush config:set system.theme default appkit --yes
```

