var gulp = require('gulp');
var sass = require('gulp-sass')(require('sass'));
var sassGlob = require('gulp-sass-glob');
var babel = require('gulp-babel');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
  return gulp.src('source/scss/**/[^_]*.scss')
  .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass())
      .on('error', function (errorInfo) {
        console.log(errorInfo.toString());
        this.emit('end');
      })
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('css'));
});

gulp.task('scripts', function() {
  return gulp.src('source/js/**/[^_]*.js')
    .pipe(sourcemaps.init())
    .pipe(babel())
    .pipe(gulp.dest('js'));
});

gulp.task('watch', function(){
  gulp.watch(['source/scss/**/*.scss','source/js/**/*.js'], gulp.series('sass','scripts'));
});
